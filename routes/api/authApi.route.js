const express = require("express");
const router = express.Router();

const authControllerAPI = require("../../controllers/api/authAPI.controller");
const passport = require('passport');

module.exports = function () {
  router.post("/authenticate", authControllerAPI.authenticate);
  router.post("/forgotPassword", authControllerAPI.forgotPassword);
  //router.post('/facebook_token', passport.authenticate('facebook-token'), authControllerAPI.authFacebookToken);

  return router;
};
