# Configuración inicial

- Se ejecuta `git init`
- Se configura `.gitignore`
- `git add -A` para agregar los archivos al stating, si los hay
- `git commit -m 'mensaje...'` para confirmar los cambios
- `git remote add origin https://user@.../user/repositorio.git` para agregar el proyecto local al repositorio remoto. Se obtiene de BitBucket
- `git push -u origin master` (para transferir cambios de lo local al servidor remoto)

## Tareas repetitivas
- `git add -A` para agregar los archivos al stating
- `git commit -m '...'` para confirmar los cambios
- `git push -u origin master` (para transferir los cambios de lo local al servidor remoto)
- Si se desea crear un tag
    - `git tag nombre-tag -m "..."` para crear el tag
    - `git push --tags`
    - `git push origin master --tags`
- Si se desea eliminar un tag
    - Local: `git tag --delete nombreTag`
    - Remoto: `git push --delete origin nombre-tag`

Para información sobre  `NPM y su configuración básica`, consulte [aquí](https://codingpotions.com/npm-tutorial).