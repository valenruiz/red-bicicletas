var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose
    //connect("mongodb://localhost:27017/red-bicicletas", {
    //connect("mongodb+srv://user-bicicletas:red-bicicletas@red-bicicletas.czcwd.mongodb.net/<dbname>?retryWrites=true&w=majority", {
    .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then((db) => {
    console.log("MongoDB conexión exitosa a " + db.connection.host);
  })
  .catch((err) => {
    console.error("MongoDB error en la conexión: " + err);
  });
